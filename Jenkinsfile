pipeline {
   agent any

   environment {
       RUNNER_HOME='/home/jenkins'
       INFRA_HOME='/home/jenkins/pipeline_projects/infrastructure'
       TESTS_HOME='/home/jenkins/pipeline_projects/soapui'
       CLUSTER_NAME='wondrous-ladybird'
       TEST_RESULTS='/home/jenkins/test_results'
       CORE_TEST_ENVIRONMENT='tests-cluster-ayan-local'
       APP_TEST_ENVIRONMENT='tests-cluster'
       REPOS_HOME='/home/jenkins/pipeline_projects'
   }
   stages {
      stage('Pre-test cleanup') {
         steps {
            notifySlack(currentBuild.result)
            sh '''
            # rm -fr ${TEST_RESULTS}
            # mkdir ${TEST_RESULTS}
            rm -f ${RUNNER_HOME}/.aerial/nomad/${CLUSTER_NAME}/connection-${CLUSTER_NAME}.env
            '''
         }
      }
      stage('Update required repositories') {
         steps {
            sh 'cd ${TESTS_HOME}; git checkout .; git checkout ci-branch; git pull'
            sh 'cd ${INFRA_HOME}; git checkout .; git checkout deploy-tests; git pull'
         }
      }
      
      stage('Prepare Nomad config'){
          steps {
            sh '''#!/bin/bash
            
            ${INFRA_HOME}/tools/set_nomad_env.sh
            source ~/.aerial/nomad/${CLUSTER_NAME}/connection-${CLUSTER_NAME}.env
            nomad job status
            
            '''  
          }
      }
      stage('Deploy latest develop jiro-api'){
          steps {
            sh '''#!/bin/bash
            source ~/.aerial/nomad/${CLUSTER_NAME}/connection-${CLUSTER_NAME}.env
            
            export JIRO_API_DOCKER_TAG=$(aws ecr describe-images --repository-name jiro-api | jq '.imageDetails |=sort_by(.imagePushedAt)' | jq '.imageDetails[] | select(.imageTags[]|test("develop")) | .imageTags[]' | grep develop | tail -1 | sed 's/"//g')
            echo "Updating jiro-api docker tag to $JIRO_API_DOCKER_TAG"
            
            jiro_api_file=${INFRA_HOME}/ha-cluster/tests/${CLUSTER_NAME}/nomad/jiro-api.nomad
            tmp=$(mktemp)
            cat $jiro_api_file | hclq set 'job.jiro-api.meta.DOCKER_TAG' $JIRO_API_DOCKER_TAG > "$tmp"
            mv "$tmp" $jiro_api_file
            
            $(nomad job plan $jiro_api_file | grep "job run")
            
            '''  
          }
      }
      stage('Wait for deployment success'){
          options {
            timeout(time: 10, unit: 'MINUTES') 
          }
          steps {
            sh '''#!/bin/bash
               source ~/.aerial/nomad/${CLUSTER_NAME}/connection-${CLUSTER_NAME}.env
               
               DEP_SUCCESS=$(nomad job status jiro-api | grep -c 'Deployment completed successfully');
               while [ $DEP_SUCCESS -lt 1 ] 
               do 
                  DEP_SUCCESS=$(nomad job status jiro-api | grep -c 'Deployment completed successfully'); 
               done
               '''
          }
      }
      stage('Commit infrastructure changes'){
          steps {
            sh '''#!/bin/bash
               cd ${INFRA_HOME}
               jiro_api_file=${INFRA_HOME}/ha-cluster/tests/${CLUSTER_NAME}/nomad/jiro-api.nomad
               
               git add $jiro_api_file
               git commit -m "updating jiro-api docker tag to latest develop build"
               git push
               '''
          }
      }
      stage('Run SoapUI Tests'){
          steps {
            
            // run core tests
            execute_test('''
            ${RUNNER_HOME}/SmartBear/ReadyAPI-3.1.0/bin/testrunner.sh \
            -r -a -j -f${TEST_RESULTS} "-RAllure Report" \
            -GAGENT_STARTUP_DELAY=60000 \
            -GAGENT_SHUTDOWN_DELAY=20000 \
            -Dtest.history.disabled=false \
            -Dsoapui.scripting.library=${TESTS_HOME}/scripts \
            -FXML -E${CORE_TEST_ENVIRONMENT} \
            ${TESTS_HOME}/Core/Aerial-CORE-API-readyapi-project
            ''')
            
            // run app tests
            execute_test('''
            ${RUNNER_HOME}/SmartBear/ReadyAPI-3.1.0/bin/testrunner.sh \
            -r -a -j -f${TEST_RESULTS} "-RAllure Report" \
            -Dtest.history.disabled=false \
            -Dsoapui.scripting.library=${TESTS_HOME}/scripts \
            -FXML -E${APP_TEST_ENVIRONMENT} \
            ${TESTS_HOME}/App/Aerial-APP-API-Auth-Users-project
            ''')
            
            // run app agent tests
            execute_test('''
            ${RUNNER_HOME}/SmartBear/ReadyAPI-3.1.0/bin/testrunner.sh \
            -r -a -j -f${TEST_RESULTS} "-RAllure Report" \
            -Dtest.history.disabled=false \
            -Dsoapui.scripting.library=${TESTS_HOME}/scripts \
            -GAGENT_STARTUP_DELAY=60000 -GAGENT_SHUTDOWN_DELAY=3000 -GSHADOW_UPDATE_DELAY=10000 \
            -FXML -E${APP_TEST_ENVIRONMENT} \
            ${TESTS_HOME}/App/Aerial-APP-API-Agents-project
            ''')
            
            
            sh '''
            echo ${PWD}
            # creating a backup of results
            cd ${RUNNER_HOME}
            export timestamp=$(date +%Y%m%d-%H%M%S-%s)
            tar -czvf test_results_${timestamp} ${TEST_RESULTS}
            
            #upload test results archive to S3 ci.report.logs bucket
            aws s3 cp test_results_${timestamp} s3://ci.report.logs/test_results_${timestamp}
            
            # currently using Allure on port 4041 in a docker container, no need for the nginx old report
            # rm -rf /var/www/html/*
            # cp -r ./test_results/* /var/www/html/
            '''
          }
      }
   }
   post {
      always {
        notifySlack(currentBuild.result)
      }
   }
}

def execute_test(test_script){
    def test_result = sh script: test_script, returnStatus: true
    if(test_result!=0 || currentBuild.result == 'UNSTABLE'){
        currentBuild.result = 'UNSTABLE'
    }else{
        currentBuild.result = 'SUCCESS'
    }
}

def notifySlack(String buildStatus = 'STARTED'){
      buildStatus =  buildStatus ?: 'STARTED'
      def colorName = 'RED'
      def colorCode = '#FF0000'
      def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
      def summary = "${subject}"
      def test_result_url = "\nFind test results on: http://192.168.11.229:4041/"
      def time_info = ""
      // Override default values based on build status
      if (buildStatus == 'STARTED') {
        color = 'WHITE'
        colorCode = '#e3f2fd'
        time_info = '\nPipeline started at '+ new Date(currentBuild.startTimeInMillis).format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        summary = "${summary} ${time_info}"
      } else if (buildStatus == 'SUCCESS') {
        color = 'GREEN'
        colorCode = '#388e3c'
        time_info = '\nPipeline completed at '+ new Date(currentBuild.startTimeInMillis+currentBuild.duration).format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        summary = "${summary} ${time_info} ${test_result_url}"
      } else if (buildStatus == 'UNSTABLE') {
        color = 'RED'
        colorCode = '#b71c1c'
        time_info = '\nPipeline completed at '+ new Date(currentBuild.startTimeInMillis+currentBuild.duration).format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        summary = "${summary} ${time_info} ${test_result_url}"
      } else {
        color = 'FUSCHIA'
        summar = "Pipeline is broken. Call your nearest QA representative"
        colorCode = '#c2185b'
      }
      // Send notifications
      slackSend color: colorCode, message: summary
}